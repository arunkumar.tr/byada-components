'use client';

import { client } from '../../../config/client';
import React, { useEffect } from 'react';

const HomePage = () => {
  useEffect(() => {
    getHomePage();
    return () => {
      // second
    };
  }, []);

  const getHomePage = async () => {
    try {
      console.log(process.env, 'accessToken');
      const res = await client?.getEntries({ content_type: 'page' });
      console.log(res.items, 'res from contentful');
    } catch (error) {
      console.log(error, 'getHomePage');
    }
  };

  return <div>HomePage</div>;
};

export default HomePage;
