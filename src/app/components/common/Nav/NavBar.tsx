'use client';
import React, { useEffect } from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';
import Container from '@mui/material/Container';

import './NavBar.module.scss';

type NavBar = {
  type?: string;
};

const NavBar: React.FC<NavBar> = () => {
  const pathname = usePathname();
  const navItems = [
    { label: 'Home', url: '/home' },
    { label: 'Get care', url: '/get-a-care' },
    { label: 'Explore careers', url: '/explore-careers' },
    { label: 'Partner with us', url: '/partner-with-us' },
    { label: 'Support us', url: '/support-us' },
    { label: 'About', url: '/about' },
  ];

  // const transparentNav = pathname === '/';

  useEffect(() => {
    console.log(pathname, '::: current active route');
    console.log(':::NavBar rendered');
    return () => {};
  }, [pathname]);

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position='static'>
          <Toolbar className='app-navbar'>
            <Container
              fixed
              sx={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <IconButton aria-label='search'>
                <svg className='svg-icon icon-24 flex items-center justify-center'>
                  <use xlinkHref='/Icons/bayada-icons.svg#search'>
                    <title>search</title>
                  </use>
                </svg>
              </IconButton>

              <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                {navItems.map((item, index) => (
                  <Button key={index} sx={{ color: 'var(--ba-primary-black)' }}>
                    <Link key={index} href={item?.url}>
                      {item.label}
                    </Link>
                  </Button>
                ))}
              </Box>

              <Typography
                variant='h6'
                noWrap
                component='div'
                sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
              >
                MUI
              </Typography>
            </Container>
          </Toolbar>
        </AppBar>
      </Box>
    </>
  );
};

export default NavBar;
